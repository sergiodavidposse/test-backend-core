# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.4](https://gitlab.com/sergiodavidposse/test-backend-core/compare/v1.0.3...v1.0.4) (2022-01-04)

### [1.0.3](https://gitlab.com/sergiodavidposse/test-backend-core/compare/v1.0.6...v1.0.3) (2021-12-31)

### [1.0.2](https://gitlab.com/sergiodavidposse/test-backend-core/compare/v1.0.6...v1.0.2) (2021-12-31)

### [1.0.1](https://gitlab.com/sergiodavidposse/test-backend-core/compare/v1.0.6...v1.0.1) (2021-12-31)

### [1.0.2](https://gitlab.com/sergiodavidposse/test-backend-core/compare/v1.0.6...v1.0.2) (2021-12-31)

### [1.0.1](https://gitlab.com/sergiodavidposse/test-backend-core/compare/v1.0.6...v1.0.1) (2021-12-31)

### [1.0.1](https://gitlab.com/sergiodavidposse/test-backend-core/compare/v1.0.6...v1.0.1) (2021-12-30)
