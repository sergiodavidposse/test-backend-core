import * as Fetch from './Fetch';
import * as Persistance from './Persistance';
export { Fetch, Persistance };
