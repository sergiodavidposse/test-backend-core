"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FetchUsers = void 0;
var FetchUsers = /** @class */ (function () {
    function FetchUsers(service) {
        this.services = new service();
    }
    FetchUsers.prototype.obtener = function () {
        return this.services.obtenerReqres('/users?page=2');
    };
    return FetchUsers;
}());
exports.FetchUsers = FetchUsers;
//# sourceMappingURL=FetchUsers.js.map