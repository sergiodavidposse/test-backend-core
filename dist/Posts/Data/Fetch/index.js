"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FetchUsers = exports.FetchPosts = void 0;
var FetchPosts_1 = require("./FetchPosts");
Object.defineProperty(exports, "FetchPosts", { enumerable: true, get: function () { return FetchPosts_1.FetchPosts; } });
var FetchUsers_1 = require("./FetchUsers");
Object.defineProperty(exports, "FetchUsers", { enumerable: true, get: function () { return FetchUsers_1.FetchUsers; } });
//# sourceMappingURL=index.js.map