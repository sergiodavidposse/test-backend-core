"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FetchPosts = void 0;
var FetchPosts = /** @class */ (function () {
    function FetchPosts(service) {
        this.services = new service();
    }
    FetchPosts.prototype.obtener = function () {
        return this.services.obtenerJsonPlaceHolder('/posts');
    };
    return FetchPosts;
}());
exports.FetchPosts = FetchPosts;
//# sourceMappingURL=FetchPosts.js.map