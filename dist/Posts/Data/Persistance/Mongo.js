"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Mongo = void 0;
var Mongo = /** @class */ (function () {
    function Mongo(service) {
        this.services = new service();
    }
    Mongo.prototype.guardar = function (post) {
        return this.services.saveMongo(post);
    };
    return Mongo;
}());
exports.Mongo = Mongo;
//# sourceMappingURL=Mongo.js.map