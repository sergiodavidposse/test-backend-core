export declare type Posts = {
    userId: number;
    id: number;
    title: string;
    body: string;
};
