"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostsModel = void 0;
var PostsModel = /** @class */ (function () {
    function PostsModel() {
        this.listar = function (posts) { return posts ? posts : false; };
        this.guardar = function (post) { return post ? post : false; };
    }
    return PostsModel;
}());
exports.PostsModel = PostsModel;
//# sourceMappingURL=PostsModel.js.map