import type { Posts } from './PostsEntity';
export interface IModel {
    listar: (posts: Posts) => Posts | boolean;
    guardar: (post: Posts) => Posts | boolean;
}
export declare class PostsModel implements IModel {
    constructor();
    listar: (posts: Posts) => false | Posts;
    guardar: (post: Posts) => false | Posts;
}
