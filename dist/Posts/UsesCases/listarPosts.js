"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UCListarPosts = void 0;
var PostsModel_1 = require("../Dominio/PostsModel");
var UCListarPosts = /** @class */ (function () {
    function UCListarPosts() {
        var _this = this;
        this.listar = function (post) {
            return _this.posts.listar(post);
        };
        //model
        this.posts = new PostsModel_1.PostsModel();
    }
    return UCListarPosts;
}());
exports.UCListarPosts = UCListarPosts;
//# sourceMappingURL=listarPosts.js.map