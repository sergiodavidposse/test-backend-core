import type { Posts } from '../Dominio/PostsEntity';
import { IModel } from '../Dominio/PostsModel';
export interface IUCGuardarPost {
    guardar: (posts: Posts) => Posts | boolean;
}
export declare class UCGuardarPosts implements IUCGuardarPost {
    protected posts: IModel;
    constructor();
    guardar: (post: Posts) => Posts | boolean;
}
