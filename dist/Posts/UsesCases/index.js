"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UCPosts = void 0;
var UCGuardarPosts = require("./guardarPosts");
var UCListarPosts = require("./listarPosts");
exports.UCPosts = {
    guardarPosts: UCGuardarPosts.UCGuardarPosts,
    listarPosts: UCListarPosts.UCListarPosts
};
//# sourceMappingURL=index.js.map