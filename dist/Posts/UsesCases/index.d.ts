import * as UCGuardarPosts from './guardarPosts';
import * as UCListarPosts from './listarPosts';
export declare const UCPosts: {
    guardarPosts: typeof UCGuardarPosts.UCGuardarPosts;
    listarPosts: typeof UCListarPosts.UCListarPosts;
};
