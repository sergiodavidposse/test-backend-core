import type { Posts } from '../Dominio/PostsEntity';
import { IModel } from '../Dominio/PostsModel';
export interface IUCListarPost {
    listar: (posts: Posts) => Posts | boolean;
}
export declare class UCListarPosts implements IUCListarPost {
    protected posts: IModel;
    constructor();
    listar: (post: Posts) => Posts | boolean;
}
