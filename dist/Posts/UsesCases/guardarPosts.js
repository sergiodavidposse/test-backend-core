"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UCGuardarPosts = void 0;
var PostsModel_1 = require("../Dominio/PostsModel");
var UCGuardarPosts = /** @class */ (function () {
    function UCGuardarPosts() {
        var _this = this;
        this.guardar = function (post) {
            return _this.posts.guardar(post);
        };
        //model
        this.posts = new PostsModel_1.PostsModel();
    }
    return UCGuardarPosts;
}());
exports.UCGuardarPosts = UCGuardarPosts;
//# sourceMappingURL=guardarPosts.js.map