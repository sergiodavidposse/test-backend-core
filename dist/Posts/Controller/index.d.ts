import type { Posts } from '../Dominio/PostsEntity';
import { UCListarPosts } from '../UsesCases/listarPosts';
import { UCGuardarPosts } from '../UsesCases/guardarPosts';
export interface IPostsService {
    obtenerData: () => Promise<Posts | boolean | string>;
}
export default class PostsService implements IPostsService {
    fetchPosts: any;
    fetchUsers: any;
    savePosts: any;
    protected ucListarPosts: UCListarPosts;
    protected ucGuardarPosts: UCGuardarPosts;
    constructor(PeticionesAdapter: any);
    obtenerData(): Promise<Posts | boolean | string>;
    obtenerUsers(): Promise<any>;
    guardarData(post: Posts): Promise<Posts | boolean | string>;
}
