import { Posts } from '../Posts/Dominio/PostsEntity';
export declare class PeticionesAdapter {
    protected configAxios: any;
    constructor();
    obtenerJsonPlaceHolder(url: string): Promise<any>;
    obtenerReqres(url: string): Promise<any>;
    saveMongo(post: Posts): Promise<string>;
}
