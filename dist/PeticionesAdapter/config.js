"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.REQRES_BASE_URL = exports.JSONPLACEHOLDER_BASE_URL = void 0;
exports.JSONPLACEHOLDER_BASE_URL = 'https://jsonplaceholder.typicode.com';
exports.REQRES_BASE_URL = 'https://reqres.in/api';
//# sourceMappingURL=config.js.map