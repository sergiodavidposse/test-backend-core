import OrdenController from './Posts/Controller';
import { entity, model } from './Posts/Dominio';
import { UCPosts as useCases } from './Posts/UsesCases';
export { OrdenController, entity, model, useCases };
export declare const obtenerData: () => Promise<{
    status: number;
    message: any;
}>;
export declare const guardarData: () => Promise<{
    status: number;
    message: any;
}>;
