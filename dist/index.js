"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.guardarData = exports.obtenerData = exports.useCases = exports.model = exports.entity = exports.OrdenController = void 0;
//CONTROLLERS
var Controller_1 = require("./Posts/Controller");
exports.OrdenController = Controller_1.default;
//DOMINIO
var Dominio_1 = require("./Posts/Dominio");
Object.defineProperty(exports, "entity", { enumerable: true, get: function () { return Dominio_1.entity; } });
Object.defineProperty(exports, "model", { enumerable: true, get: function () { return Dominio_1.model; } });
//CASOS DE USO
var UsesCases_1 = require("./Posts/UsesCases");
Object.defineProperty(exports, "useCases", { enumerable: true, get: function () { return UsesCases_1.UCPosts; } });
////FRAMEWORK-------------------------------------------------------------------------
var post = {
    id: 1,
    title: 'test post',
    userId: 1,
    body: 'test post this is test post'
};
var Peticiones_1 = require("./PeticionesAdapter/Peticiones");
var ordenesService = new Controller_1.default(Peticiones_1.PeticionesAdapter);
// let t = new PeticionesAdapter();
// (async()=>{
//   try{
//     let g = await t.obtenerJsonPlaceHolder('/posts');
//     console.log(g);
//   } catch (er){
//     console.log(er);
//   }
// })();
////RUTA
var obtenerData = function () { return __awaiter(void 0, void 0, void 0, function () {
    var data, e_1, message;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, ordenesService.obtenerData()];
            case 1:
                data = _a.sent();
                return [2 /*return*/, { status: 200, message: data }];
            case 2:
                e_1 = _a.sent();
                message = { status: 500, message: e_1 };
                return [2 /*return*/, message];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.obtenerData = obtenerData;
var guardarData = function () { return __awaiter(void 0, void 0, void 0, function () {
    var data, e_2, message;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, ordenesService.guardarData(post)];
            case 1:
                data = _a.sent();
                return [2 /*return*/, { status: 200, message: data }];
            case 2:
                e_2 = _a.sent();
                message = { status: 500, message: e_2 };
                return [2 /*return*/, message];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.guardarData = guardarData;
//RUNTIME SIMULATION
// (async()=>{
//   console.log('Llamando a obtenerData');
//   let resultados = await guardarData();
//   console.log(resultados.message);
// })();
//# sourceMappingURL=index.js.map