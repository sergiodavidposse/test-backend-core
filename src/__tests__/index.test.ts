import { obtenerData, guardarData, entity } from '../index';

jest.setTimeout(15000);

interface IResponse {
  status: number;
  message: string;
}

let response: IResponse;
let data: any;

beforeEach(async () => {
  response = await obtenerData();
  data = await guardarData();
});

describe('Testing obtenerData', () => {
  it('Response debe contener un objeto con status 200', async () => {
    expect(response.status).toBe(200);
    expect(data.status).toBe(200);
  });
});
describe('Testing guardarData', () => {
  it('Response debe contener un objeto con status 200', async () => {
    expect(data.status).toBe(200);
  });
});
