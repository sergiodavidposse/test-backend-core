import type { Posts } from '../Dominio/PostsEntity';
import { IModel, PostsModel } from '../Dominio/PostsModel';

export interface IUCListarPost {
  listar: (posts: Posts) => Posts | boolean;
}

export class UCListarPosts implements IUCListarPost {
  protected posts: IModel;

  constructor() {
    //model
    this.posts = new PostsModel();
  }
  listar = (post: Posts): Posts | boolean => {
    return this.posts.listar(post);
  };
}
