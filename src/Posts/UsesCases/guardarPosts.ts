import type { Posts } from '../Dominio/PostsEntity';
import { IModel, PostsModel } from '../Dominio/PostsModel';

export interface IUCGuardarPost {
  guardar: (posts: Posts) => Posts | boolean;
}

export class UCGuardarPosts implements IUCGuardarPost {
  protected posts: IModel;

  constructor() {
    //model
    this.posts = new PostsModel();
  }

  guardar = (post: Posts): Posts | boolean => {
    return this.posts.guardar(post);
  };
}
