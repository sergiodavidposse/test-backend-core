import * as UCGuardarPosts from './guardarPosts';
import * as UCListarPosts from './listarPosts';

export const UCPosts = {
  guardarPosts: UCGuardarPosts.UCGuardarPosts,
  listarPosts: UCListarPosts.UCListarPosts,
};
