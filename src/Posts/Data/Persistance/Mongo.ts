import { Posts } from '../../Dominio/PostsEntity';

export class Mongo {
  services: any;
  constructor(service: any) {
    this.services = new service();
  }
  guardar(post: Posts) {
    return this.services.saveMongo(post);
  }
}
