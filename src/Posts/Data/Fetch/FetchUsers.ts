export class FetchUsers {
  services: any;
  constructor(service: any) {
    this.services = new service();
  }
  obtener() {
    return this.services.obtenerReqres('/users?page=2');
  }
}
