import { Posts } from '../../Dominio/PostsEntity';

export class FetchPosts {
  services: any;
  constructor(service: any) {
    this.services = new service();
  }
  obtener(): Posts {
    return this.services.obtenerJsonPlaceHolder('/posts');
  }
}
