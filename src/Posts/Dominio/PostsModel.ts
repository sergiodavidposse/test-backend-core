import type { Posts } from './PostsEntity';
import * as mongo from '../Data/Persistance/Mongo';

//Modelo

export interface IModel {
  listar: (posts: Posts) => Posts | boolean;
  guardar: (post: Posts) => Posts | boolean;
}

export class PostsModel implements IModel {
  constructor() {}
  listar = (posts: Posts) => (posts ? posts : false);
  guardar = (post: Posts) => (post ? post : false);
}
