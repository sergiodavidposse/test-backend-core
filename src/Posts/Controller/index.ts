//ENTITY POSTS
import type { Posts } from '../Dominio/PostsEntity';
//USE CASES
import { UCPosts } from '../UsesCases';
import { UCListarPosts } from '../UsesCases/listarPosts';
import { UCGuardarPosts } from '../UsesCases/guardarPosts';
//DATA
import * as Data from '../Data';

export interface IPostsService {
  obtenerData: () => Promise<Posts | boolean | string>;
}

export default class PostsService implements IPostsService {
  fetchPosts: any;
  fetchUsers: any;
  savePosts: any;

  protected ucListarPosts: UCListarPosts;
  protected ucGuardarPosts: UCGuardarPosts;

  constructor(PeticionesAdapter: any) {
    this.ucListarPosts = new UCPosts.listarPosts();
    this.ucGuardarPosts = new UCPosts.guardarPosts();

    this.fetchPosts = new Data.Fetch.FetchPosts(PeticionesAdapter);
    this.fetchUsers = new Data.Fetch.FetchUsers(PeticionesAdapter);
    this.savePosts = new Data.Persistance.Mongo(PeticionesAdapter);
  }

  async obtenerData(): Promise<Posts | boolean | string> {
    try {
      let res = await this.fetchPosts.obtener();
      //paso la response al caso de uso
      const result = this.ucListarPosts.listar(res);
      return result;
    } catch (e) {
      console.log(e);
      return 'No se pudo obtener data!';
    }
  }

  //USER NO USA ENTIDAD
  async obtenerUsers(): Promise<any> {
    try {
      let res = await this.fetchUsers.obtener();
      return res;
    } catch (e) {
      return 'No se obtuvieron los users';
    }
  }

  async guardarData(post: Posts): Promise<Posts | boolean | string> {
    try {
      const result = this.ucGuardarPosts.guardar(post);
      return result;
    } catch (e) {
      return 'No se pudo guardar data!';
    }
  }
}
