//CONTROLLERS
import OrdenController from './Posts/Controller';
//DOMINIO
import { entity, model } from './Posts/Dominio';
//CASOS DE USO
import { UCPosts as useCases } from './Posts/UsesCases';

export { OrdenController, entity, model, useCases };

////FRAMEWORK-------------------------------------------------------------------------
const post: entity.Posts = {
  id: 1,
  title: 'test post',
  userId: 1,
  body: 'test post this is test post',
};

import { PeticionesAdapter } from './PeticionesAdapter/Peticiones';

const ordenesService = new OrdenController(PeticionesAdapter);

// let t = new PeticionesAdapter();
// (async()=>{
//   try{
//     let g = await t.obtenerJsonPlaceHolder('/posts');
//     console.log(g);
//   } catch (er){
//     console.log(er);
//   }
// })();

////RUTA
export const obtenerData = async () => {
  try {
    const data = await ordenesService.obtenerData();
    return { status: 200, message: data };
  } catch (e: any) {
    let message = { status: 500, message: e };
    return message;
  }
};

export const guardarData = async () => {
  try {
    const data = await ordenesService.guardarData(post);
    return { status: 200, message: data };
  } catch (e: any) {
    let message = { status: 500, message: e };
    return message;
  }
};

//RUNTIME SIMULATION
// (async()=>{
//   console.log('Llamando a obtenerData');
//   let resultados = await guardarData();
//   console.log(resultados.message);
// })();
