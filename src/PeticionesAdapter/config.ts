export const JSONPLACEHOLDER_BASE_URL = 'https://jsonplaceholder.typicode.com';
export const REQRES_BASE_URL = 'https://reqres.in/api';
