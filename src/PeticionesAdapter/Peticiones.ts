import axios from 'axios';
import { Posts } from '../Posts/Dominio/PostsEntity';
import { REQRES_BASE_URL, JSONPLACEHOLDER_BASE_URL } from './config';

export class PeticionesAdapter {
  protected configAxios: any;

  constructor() {
    this.configAxios = (baseUrl: any) => {
      let instance = axios.create({
        baseURL: baseUrl,
      });
      return instance;
    };
  }

  async obtenerJsonPlaceHolder(url: string) {
    let instance = this.configAxios(JSONPLACEHOLDER_BASE_URL);
    return await instance.get(url);
  }

  async obtenerReqres(url: string) {
    let instance = this.configAxios(REQRES_BASE_URL);
    return await instance.get(url);
  }

  async saveMongo(post: Posts) {
    //instancia de mongo
    return `saving mongo simulation...title: ${post.title}`;
  }
}
